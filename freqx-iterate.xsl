<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0" expand-text="yes"
  xmlns:dc="http://www.delightfulcomputing.com/"
  xmlns:err="http://www.w3.org/2005/xqt-errors"
  xmlns:file="http://expath.org/ns/file"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:saxon="http://saxon.sf.net/"
  exclude-result-prefixes="#all"
  >

  <!--* The inital development of freqx was sponsored by Mulberry Technologies
      * for which i'm  very grateful.
      *
      * Licence is CC By-SA, which means please link back and say
      * where you got it.
      *-->
  <xd:doc>
    <xd:desc>
      <xd:p>Extract element names, attribute names, namespaces,
      attribute values, from a collection of input documents.
      Data is collected in XML, and an HTML report can also be
      generated.</xd:p>
      <xd:p>You can keep specific attributes or values out of the
      report using an attribute value map.</xd:p>
      <xd:param name="collection">
	<xd:p>This must be either a path to a directory or
	<xd:i>#none</xd:i>.  You can use .
	for your current working directory.</xd:p>
	<xd:p>Use #none if you want to use only
	  <xd:i>collection-uri</xd:i>
	</xd:p>
      </xd:param>
      <xd:param name="pattern">
	<xd:p>If your collection points to a directory,
	this is a pattern to match files to process. The default
	is <xd:i>*.xml</xd:i> but you could also write
	<xd:i>*.(xml|xslt|svg)</xd:i> for example.</xd:p>
      </xd:param>
      <xd:param name="recurse">
	<xd:p>Whether to look in sub-folders (subdirectories) if
	the collection points to a folder. Defaults to yes; use no
	to look only in the folder named and not sub-folders.</xd:p>
      </xd:param>
      <xd:param name="collection-uri">
        <xd:p>The name of a file (can be a URL) containing a Saxon-format
	collection file, with a doc element for each file you want
	to process, each having an <xd:i>href</xd:i> attribute to find
	its document.</xd:p>
	<xd:p>As an extension, change href= to exclude= and the corresponding
	file (based on an exact match of the filename)
	will not be loaded even via the
	<xd:i>collection</xd:i> parameter.</xd:p>
      </xd:param>
      <xd:param name="oxygen-project-file">
        <xd:p>The name of an Oxygen XML Editor project file; the filename
	usualy ends in an <xd:i>.xpr</xd:i> extension. Explicit
	<xd:i>file</xd:i> elements found anywhere under the
	<xd:i>projectTree</xd:i> element will be used.</xd:p>
	<xd:p>You can combine this with the
	<xd:i>write-collection-file-uri</xd:i> to make a collection
	file to use with the <xd:i>collecion-uri</xd:i> parameter.</xd:p>
	<xd:p>Note that the XPR file should be on the local system,
	rather than be accessed by http, unless all the files it mentions
	are also accessible by http(or ftp or https and so on).</xd:p>
      </xd:param>
      <xd:param name="max-docs">
        <xd:p>Set this to a number to include only the first
	<xd:i>max-docs</xd:i> documents in the report. This can
	make the process run faster, and use less memory.</xd:p>
      </xd:param>
      <xd:param name="max-file-bytes">
	<xd:p>If the <xd:i>file:size()</xd:i> function is
	available, input files larger than
	<xd:i>max-file-bytes</xd:i> are not loaded, and also
	do not contribute towards the total number of documents
	processed for <xd:i>max-docs</xd:i>.</xd:p>
      </xd:param>
      <xd:param name="include-error-details">
	<xd:p>Set this to yes or no to include all error messages trying
	to load documents, to <xd:i>no</xd:i> to exclude the error messages,
	and set it to a number to include errors  from  parsing at most
	that many documents; the default is 50.</xd:p>
	<xd:p>You should normally set this to <xd:i>no</xd:i> before sending the
	report to someone else, as parsing errors may include full paths to files
	as well as snippets of content.</xd:p>
      </xd:param>
      <xd:param name="merge-counts-from">
	<xd:p>Load an XML FreqX document of counts and merge results into it.</xd:p>
	<xd:p>Errors from  previous runs are not carried forward.</xd:p>
      </xd:param>
      <xd:param name="xml-output-uri">
        <xd:p>The result filename, or <xd:i>#none</xd:i> for no data written.</xd:p>
	<xd:p>If this is relative, such as <xd:i>report.xml</xd:i>,
	the data file will be created in the
	same folder as the XSLT stylesheet.</xd:p>
      </xd:param>
      <xd:param name="html-output-uri">
        <xd:p>Filename for the HTML report, or <xd:i>#none</xd:i> for no report.</xd:p>
	<xd:p>If this is relative, such as "report.html", the report will be created in the
	same folder as the XSLT stylesheet.</xd:p>
	<xd:p>Use <xd:i>#none</xd:i> (the default) if you do not want the HTML report file generated.</xd:p>
      </xd:param>
      <xd:param name="csv-output-uri">
        <xd:p>Filename for the spreadsheet (CSV, Comma-Separated Value) report, or <xd:i>#none</xd:i> for no report.</xd:p>
	<xd:p>If this is relative, such as "report.csv", the report will be created in the
	same folder as the XSLT stylesheet.</xd:p>
	<xd:p>Use <xd:i>#none</xd:i> (the default) if you do not want the CSV report file generated.</xd:p>
      </xd:param>
      <xd:param name="html-output-css-uri">
	<xd:p>The name of the CSS file that the HTML report should
	link to; a Web browser will take this as relative to the HTML,
	not to the stylesheet. The default is
	<xd:i>lib/html-output-css-uri.css</xd:i> and you can read that file
	in the distribution for more information.</xd:p>
      </xd:param>
      <xd:param name="csv-output-separate-files">
	<xd:p>A space-separated list of tokens chosen from the
	following list:</xd:p>
      <xd:p>elements element-parents attributes attribute-values
            text-elements all</xd:p>
      <xd:p>The parameter is used only if $csv-output-uri is neither empty
      nor set to #none, and in that case the individual reports requested
      are put into files with the report name just before a .csv suffix
      (which is added if not supplied).</xd:p>
      <xd:p>The value <xd:i>help</xd:i> will produce a list of
      recognized tokens.</xd:p>
      </xd:param>
      <xd:param name="write-collection-file-uri">
         <xd:p>Filename for the list of documents processed, in the
	 format that can be used as a collection parameter.</xd:p>
      </xd:param>
      <xd:param name="freqx-control-file">
      </xd:param>
	<xd:p>This should point to a FreqX control file or have
	the exact value <xd:i>#none</xd:i> - the default is to use
	<xd:i>freqx-control.xml</xd:i> in the same folder as the stlyesheet.
	Use a FreqX control file to say any of:</xd:p>
	<xd:p>Process attributes and elements normally;</xd:p>
	<xd:p>Ignore attributes and/or elements with a specific (namespace, local-name)
	combination (namespace name can be empty, or can be * for any namespace name);</xd:p>
	<xd:p>Ignore all attributes and/or elements with a specific namespace name;</xd:p>
	<xd:p>Instead of ignoring (dropping from the report), it's also
	  possible to process attributes and elements selectively:</xd:p>
	<xd:p>include an attribute but not its value</xd:p>
	<xd:p>replace an attribute value by a constant one, e.g. "ID"</xd:p>
	<xs:p>Ignore the presence of text in an element</xs:p>
	<xs:p>Ignore all children of an element</xs:p>
      <xd:p>The default in the sample file distributed with FreqX is for any
      attribute whose local name is "id" or "rid" to
      have its value changed to "#id-value".</xd:p>
      <xd:p>See freqx-control.xml for more details.</xd:p>
      <xd:param name="process-file-xslt">
	<xd:p>The main freqx stylesheet uses the XSLT stylesheet specified
	with <xd:i>process-file-xsl</xd:i> to get a namespace-list element
	and a sequence of count elements for each file to be processed.</xd:p>
	<xd:p>The default is <xd:i>freqx-process-file.xsl</xd:i>
	and should be in the same folder as the main stylesheet. See the comments
	in that default file for more information.</xd:p>
      </xd:param>
    </xd:desc>
  </xd:doc>
  <xsl:param name="collection" as="xs:string?" select=" 'samples' " />
  <xsl:param name="collection-uri" as="xs:string?" select=" '#none' " />
  <xsl:param name="oxygen-project-file" as="xs:string?" select=" '#none' " />
  <xsl:param name="pattern" as="xs:string?" select=" '*.xml' " />
  <xsl:param name="recurse" as="xs:string?" select=" 'yes' " />
  <xsl:param name="max-docs" as="xs:string?" select=" '' " />
  <xsl:param name="max-file-bytes" as="xs:string?" select=" '' " />
  <xsl:param name="include-error-details" as="xs:string" select=" '50' " />
  <xsl:param name="merge-counts-from" as="xs:string?" select=" () " />
  <xsl:param name="freqx-control-file" as="xs:string" select=" 'freqx-control-file.xml' " />
  <xsl:param name="xml-output-uri" as="xs:string" select=" 'freqx-saved-data.xml' " />
  <xsl:param name="write-collection-file-uri" as="xs:string" select=" '#none' " />
  <xsl:param name="csv-output-uri" as="xs:string" select=" 'freqx-html-report.csv' " />
  <xsl:param name="csv-output-separate-files" as="xs:string" select=" '' " />
  <xsl:param name="html-output-uri" as="xs:string" select=" 'freqx-html-report.html' " />
  <xsl:param name="html-output-css-uri" as="xs:string" select=" 'lib/freqx-html-styles.css' "/>
  <xsl:param name="process-file-xsl" as="xs:string" select=" 'freqx-process-file.xsl' "/>
  <xsl:variable name="max-docs-int" select="if ($max-docs eq '') then 0 else xs:integer($max-docs)" />
  <xsl:variable name="max-file-bytes-int" select="if ($max-file-bytes eq '') then 0 else xs:integer($max-file-bytes)" />

  <!--* Use shallow-skip to avoid processing text nodes: *-->
  <xsl:mode on-no-match="shallow-skip" />

  <!--* namespace mapping; the XML namespacing is predeclared *-->
  <xsl:accumulator name="namespaces" as="map(xs:string, xs:string)"
    initial-value="map { 'http://www.w3.org/XML/1998/namespace' : '1' } "
    >
    <xsl:accumulator-rule match="*|@*"
      select="
	if (exists($value(namespace-uri(.)))) then $value
	else map:put($value,
	  xs:string(namespace-uri(.)), xs:string(map:size($value) + 1)
	)
      "/>
  </xsl:accumulator>

  <!--* Newline (hexadecimal 0A, decimal 10, control-J, ASCII NL): *-->
  <xsl:variable name="NL" as="xs:string" select=" '&#xa;' "/>


  <xsl:variable name="freqx-control-doc" as="element(freqx-control)?"
    select="if (dc:none($freqx-control-file))
            then ()
	    else doc($freqx-control-file)/freqx-control" />

  <!--*****************************************************************
      *
      * The main initial template is called xsl:initial-template
      *
      * Run Saxon with -it:xsl:initial-template
      *
      *-->
  <xd:doc>
    <xd:desc>
      <xd:p>This is the initial template for calling FreqX.</xd:p>
      <xd:p>Use <xd:i>saxon -it '{http://www.w3.org/1999/XSL/Transform}initial-template'</xd:i> or just <xd:i>saxon -it xsl:initial-template</xd:i>  to invoke it.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template name="xsl:initial-template">

    <!--* We will process all the input documents and build up separate
        * sets of counts, e.g. one for how many of each distinct element
	* name (inclding namespace URI but not prefix) we have seen.
	* To avoid having to keep every occurrence of every element in
	* memory, the counts are coalesced after each file.
	* We store the counts in a variable so we can produce multiple
	* reports, and so we can work out maximums in advance e.g. to
	* make usable bar charts.
	*-->
    <xsl:variable name="previous" as="element(freqx)?"
      select="if (not(empty($merge-counts-from)))
              then doc($merge-counts-from)/freqx
	      else ()" />

    <xsl:variable name="counts" as="element(freqx)">
      <freqx>
	<xsl:variable name="file-list"
	  select="
	    if (dc:none($collection-uri))
	    then ()
	    else doc($collection-uri)" />

	<!--* Make a map whose keys are filenames to exclude and
	    * whose values are true(), so that we can quickly look
	    * up whether we are excluding a file; this is so we
	    * can run on tens of thousands of input fies. Maybe this
	    * could as well be a simple sequence, as i didn't time it.
	    *-->
	<xsl:variable name="exclude" as="map(xs:string, xs:boolean)"
	  select="map:merge(
	    $file-list//*/@exclude ! map:entry(xs:string(.), true())
	  )" />

	<xsl:variable name="docs-to-try" as="map(*)*">
	  <xsl:if test="not( dc:none($oxygen-project-file) )">
	    <xsl:call-template name="read-oxygen-project-file">
	      <xsl:with-param name="filename" select="$oxygen-project-file" />
	    </xsl:call-template>
	  </xsl:if>

	  <xsl:if test="not(dc:none($collection))">
	    <xsl:variable name="sep" as="xs:string"
	      select="if (contains($collection, '?')) then ';' else '?'" />

	    <xsl:variable name="collection-argument" as="xs:string"
	      select="$collection || $sep || 'select=' || $pattern || ';stable=no;recurse=' || $recurse || ';metadata=yes'" />

	    <!--* calling collection() here may produce warnings about
	        * invalid documents and they may not be included in
		* the variable, but uri-collection() doesn't do the
		* same metadata thing
		*-->
	    <xsl:message>try collection {$collection-argument}</xsl:message>
	    <xsl:sequence select="collection($collection-argument) (: [
		.?content-type = 'application/xml'
		] :)" />
	  </xsl:if>

	  <xsl:if test="exists($file-list)">
	    <xsl:sequence select="$file-list//doc/@href ! (
		let $dot := xs:string(.)
		return map {
		  'name' : $dot, 
		  'content-type' : 'application/xml'
		  }
		)" />
	  </xsl:if>
	</xsl:variable>

	<xsl:variable name="docs-not-excluded" as="map(*)*"
	  select="$docs-to-try[not($exclude(.?name))]" />

	<xsl:message>docs to try {string-join($docs-not-excluded ! ?name, ', ')}</xsl:message>

	<xsl:iterate select="1 to count($docs-not-excluded)">
	  <xsl:param name="elements-seen" as="element(count)*" select="$previous/elements/count" />
	  <xsl:param name="element-parents-seen" as="element(count)*" select="$previous/element-parents/count" />
	  <xsl:param name="text-seen" as="element(count)*" select="$previous/text/count" />
	  <xsl:param name="attributes-seen" as="element(count)*" select="$previous/attributes/count" />
	  <xsl:param name="attribute-values-seen" as="element(count)*" select="$previous/attribute-values/count" />
	  <xsl:param name="docs-seen" as="element(doc)*" select="$previous/docs/doc" />
	  <!--* do not include errors from previous runs: *-->
	  <xsl:param name="errors-seen" as="element(fail)*" select=" () " />

	  <xsl:param name="docno"  as="xs:integer" select="0" />

	  <!--* we are going to make lists of
	      * All elements seen, with numbers of documents for each,
	      * and whether they have text, and their parent elements;
	      * All attributes seen, with numbers of documents for each,
	      * and their values and parent elements
	      * Documents processed
	      * The first 50 (or all) failures with reasons
	      *
	      * We will merge these after harvesting them from each document,
	      * so we don't need to keep in memory a record of every
	      * occurrence of every element, but only totals.
	      *
	      * Note: i tried using maps rather than elements and Saxon 9-EE
	      * took longer and ran out of memory with the sample set i had.
	      *-->

	  <xsl:on-completion>
	    <!--* CAUTION the following list of elements and variables
		* occurs in three places, here and in xsl:on-completion,
		* an in next-iteration.
		* It would be a better design to use a map to hold
		* everything, and to have a single function or template
		* to make this list (Don't Repeat Yourself), but older
		* versions of Saxon aren't good at garbage collecting maps
		* it seems.
		*-->
	    <elements>
	      <xsl:sequence select="$elements-seen" />
	    </elements>
	    <element-parents>
	      <xsl:sequence select="$element-parents-seen" />
	    </element-parents>
	    <text>
	      <xsl:sequence select="$text-seen" />
	    </text>
	    <attributes>
	      <xsl:sequence select="$attributes-seen" />
	    </attributes>
	    <attribute-values>
	      <xsl:sequence select="$attribute-values-seen" />
	    </attribute-values>
	    <docs>
	      <xsl:sequence select="$docs-seen" />
	    </docs>
	    <!--* no need to keep details of errors around if
		* we do not want them in the final report:
		*-->
	    <xsl:if test="not($include-error-details = 'no')">
	      <errors>
		<xsl:sequence select="$errors-seen" />
	      </errors>
	    </xsl:if>
	  </xsl:on-completion>

	  <xsl:variable name="this" as="map(*)" select="$docs-not-excluded[position() = current()]" />

	  <xsl:message>iterate ({.} of {count($docs-to-try)}): {$this?name} of type {$this?content-type}...</xsl:message>

	  <xsl:variable name="this-data-set" as="element(*)*">
	    <xsl:try>
	      <xsl:choose>
		<xsl:when test="not($this?content-type = ('application/xml', 'text/html'))">
		  <fail doc="{$this?name}" reason="freqx did not attempt to load non-XML document" />
		</xsl:when>
		<xsl:when test="($max-docs-int ne 0) and
		  ($docno ge $max-docs-int)
		">
		  <fail doc="{$this?name}" reason="freqx: skipped [max number]" />
		</xsl:when>
		<xsl:when test="($max-file-bytes-int ne 0) and
		    (
		      if (function-available('file:size'))
		      then (file:size($this?name) gt $max-file-bytes-int)
		      else false()
		    )">
		  <fail doc="{$this?name}" reason="freqx: skipped [max size is {$max-file-bytes-int} and file size is {file:size($this?name)}]" />
		</xsl:when>
		<xsl:otherwise>
		  <xsl:message>process {$this?name}...</xsl:message>
		  <doc name="{$this?name}" />
		  <xsl:sequence select="transform(
		    map {
		      'stylesheet-location' : $process-file-xsl,
		      'source-node' :
			if (function-available('saxon:discard-document'))
			then saxon:discard-document(doc($this?name))
			else doc($this?name),
		      'stylesheet-params' : map {
			QName('http://www.delightfulcomputing.com/', 'freqx-control-doc') : $freqx-control-doc
  
		      }
		    }
		  )?output/*" />
		  <!--*
		  <xsl:apply-templates select="$this?fetch()/*"/>
		  *-->
		</xsl:otherwise>
	      </xsl:choose>
	      <xsl:catch>
		<fail doc="{$this?name}" reason="{$err:description} line {$err:line-number} v {$err:value} c {$err:column-number}" />
	      </xsl:catch>
	    </xsl:try>
	  </xsl:variable>

	  <xsl:variable name="failed" as="xs:boolean"
		select="exists($this-data-set[local-name() = 'fail'])" />

	  <xsl:choose>
	  <xsl:when test="($max-docs-int ne 0) and ($docno gt $max-docs-int)">
	    <xsl:break>
	      <!--* CAUTION the following list of elements and variables
		  * occurs in three places, here and in xsl:on-completion,
		  * an in next-iteration.
		  *
		  * It would be a better design to use a map to hold
		  * everything, and to have a single function or template
		  * to make this list (Don't Repeat Yourself), but older
		  * versions of Saxon aren't good at garbage collecting maps
		  * it seems.
		  *-->
	      <elements>
		<xsl:sequence select="$elements-seen" />
	      </elements>
	      <text>
		<xsl:sequence select="$text-seen" />
	      </text>
	      <element-parents>
		<xsl:sequence select="$element-parents-seen" />
	      </element-parents>
	      <attributes>
		<xsl:sequence select="$attributes-seen" />
	      </attributes>
	      <attribute-values>
		<xsl:sequence select="$attribute-values-seen" />
	      </attribute-values>
	      <docs>
		<xsl:sequence select="$docs-seen" />
	      </docs>
	      <!--* no need to keep details of errors around if
	          * we do not want them in the final report:
		  *-->
	      <xsl:if test="not($include-error-details = 'no')">
		<errors>
		  <xsl:sequence select="$errors-seen" />
		</errors>
	      </xsl:if>
	    </xsl:break>
	  </xsl:when>
	  <xsl:otherwise>

	  <xsl:next-iteration>
	    <xsl:with-param name="elements-seen"
	      select="dc:merge-elements($elements-seen, $this-data-set[local-name() = 'e'], 'e')" />
	    <xsl:with-param name="element-parents-seen"
	      select="dc:merge-element-parents($element-parents-seen, $this-data-set[local-name() = 'e'], 'e') " />
	    <xsl:with-param name="text-seen"
	      select="dc:merge-elements($elements-seen, $this-data-set[local-name() = 't'], 't')" />
	    <xsl:with-param name="attributes-seen"
	      select="dc:merge-elements($attributes-seen, $this-data-set[local-name() = 'at'], 'at')" />
	    <xsl:with-param name="attribute-values-seen"
	      select="
	        if ( ( position() mod 100 ) eq 99 )
		then
		  dc:merge-attribute-values(
		    $attribute-values-seen,
		    $this-data-set[local-name() = 'at']
		  )
		else (
		  $attribute-values-seen,
		  dc:merge-attribute-values(
		    (),
		    $this-data-set[local-name() = 'at']
		  )
		)
	      " />
	    <xsl:with-param name="docs-seen"
	      select="($docs-seen , ( $this-data-set[local-name() = 'doc']))" />
	    <!--* simply accumulate all the errors *-->
	    <!--* Only the first 50 *-->
	    <xsl:with-param name="errors-seen"
	      select="($errors-seen , ($this-data-set[local-name() = 'fail']))[position() le 50]" />
	    <xsl:with-param name="docno"
	      select="if ($failed) then $docno else $docno +  1" />
	  </xsl:next-iteration>
	  </xsl:otherwise>
	  </xsl:choose>
	</xsl:iterate>
      </freqx>
    </xsl:variable>

    <output>
      <xsl:if test="$xml-output-uri ne ''">
	<xsl:result-document href="{$xml-output-uri}">
	  <freqx saved-on="{current-dateTime()}">
	    <xsl:sequence select="$counts/*[
	      not(
		(local-name() eq errors) and
		($include-error-details = 'no')
	      )
	    ]" />
	  </freqx>
	</xsl:result-document>
	<p>XML output written to {$xml-output-uri}</p>
	<xsl:text>&#xa;</xsl:text>
      </xsl:if>

      <xsl:if test="not( dc:none($write-collection-file-uri) )">

	<xsl:call-template name="make-collection-file">
	  <xsl:with-param name="counts" select="$counts" />
	</xsl:call-template>
      </xsl:if>

      <xsl:if test="not( dc:none($html-output-uri) )">
	<xsl:call-template name="make-html-report">
	  <xsl:with-param name="counts" select="$counts" />
	</xsl:call-template>
	<xsl:text>&#xa;</xsl:text>
      </xsl:if>

      <xsl:if test="not(  dc:none($csv-output-uri) )">
	<xsl:call-template name="make-csv-report">
	  <xsl:with-param name="counts" select="$counts" />
	</xsl:call-template>
	<xsl:text>&#xa;</xsl:text>
      </xsl:if>

      <div class="summary" date="{current-date()}">
	<p>Total documents: {
	  count($counts/docs/*) + count($counts/errors/fail)
	}.</p>
	<xsl:text>&#xa;</xsl:text>
	<xsl:text>&#xa;</xsl:text>
	<p>Documents with errors, not included: {count($counts/errors/fail)}</p>
	<xsl:text>&#xa;</xsl:text>
	<p>Processed successfully: {count($counts/docs/*)}.</p>
	<xsl:text>&#xa;</xsl:text>
      </div>
    </output>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Read an Oxygen project file and fish out filenames.</xd:p>
      <xd:p>Tested with project files for Oxygen XML Editor
      version 20.1 only.</xd:p>
    </xd:desc>
    <xd:param name="filename">
      <xd:p>The project file (*.xpr) to process. This should be a local
      file, or, files and folders mentioned in it should be accessible
      to us.</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:template name="read-oxygen-project-file" as="map(*)*">
    <xsl:param name="filename" as="xs:string" />

    <!--* fatal error if xpr file not found seems reasonable: *-->
    <xsl:variable name="xpr" select="doc($filename)" />

    <xsl:apply-templates select="$xpr//project/projectTree/*" mode="xpr">
      <xsl:with-param name="path-so-far" select="base-uri($xpr)" />
    </xsl:apply-templates>

  </xsl:template>
  <xsl:mode name="xpr" on-no-match="shallow-skip" />

  <xd:doc>
    <xd:desc>
      <xd:p>Like string-join but puts the separator after each item
      instead of between them, so that
      dc:string-append-each( ('a', 'b', 'c'), '/')
      gives you a/b/c/
      but with empty input you get an empty sequence back.
      </xd:p>
    </xd:desc>
    <xd:param name="input">
      <xd:p>A sequence of strings</xd:p>
    </xd:param>
    <xd:param name="separator">
      <xd:p>The constant stringto put after each one</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:function name="dc:string-append-each" as="xs:string?">
    <xsl:param name="input" as="xs:string*" />
    <xsl:param name="separator" as="xs:string" />

    <xsl:sequence select="
      string-join($input, $separator) ||
      ( if (empty($input)) then '' else $separator )
      "/>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Internal template to process an XPR file</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="file" mode="xpr" as="map(*)*">

    <xsl:variable name="base" as="xs:string"
      select="replace(base-uri(ancestor::projectTree), '/[^/]+$', '')" />

    <xsl:sequence select="
	  (
	    $base || '/' || (
	      dc:string-append-each(reverse(ancestor::folder/@name), '/')
	    ) ||  (: value so far ends in a / :)
	    xs:string(@name)
	  ) ! (
	    let $dot := xs:string(.)
	    return map {
	      'name' : $dot, 
	      'content-type' : 'application/xml'
	    }
	    )
	  "/>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Constructs attribute nodes to capture the name
      of the given node and its parent as parent-node</xd:p>
    </xd:desc>
    <xd:param name="node">
      <xd:p>The node should be an element. You must handle the document node
      before calling this, as otherwise you would make duplicate
      entries for the top-level element and its ancestors.</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:template name="make-parent-attributes" as="attribute(*)*">
    <xsl:param name="node" as="element(*)" />
    <xsl:param name="nsmap" as="map(xs:string, xs:string)" />

    <xsl:attribute name="parent-name" select="local-name($node)" />
    <xsl:if test="namespace-uri($node)">
      <xsl:attribute name="parent-ns"
	select="$nsmap(namespace-uri($node))" />
    </xsl:if>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Match element nodes and turn them into un-nested e and
      (optionally) t elements so we can count them later.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="*">
    <xsl:variable name="rewrite" select="dc:get-rewrite(.)" />

    <xsl:if test="not( contains-token($rewrite/@drop, 'all') )">
      <!--* we want to know at least something about this element *-->
      <e name="{local-name(.)}">
	<xsl:if test="namespace-uri(.)">
	  <xsl:if test="not(contains-token($rewrite/@drop, 'namespace'))">
	    <xsl:attribute name="ns"
	      select="accumulator-before('namespaces')(namespace-uri(.))" />
	  </xsl:if>
	</xsl:if>
	<xsl:if test="not(contains-token($rewrite/@drop, 'parent'))">
	  <xsl:choose>
	    <xsl:when test=".. instance of document-node()">
	      <xsl:attribute name="parent-name" select=" '#root' " />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:call-template name="make-parent-attributes">
		<xsl:with-param name="node" select=".." />
		<xsl:with-param name="nsmap" select="accumulator-after('namespaces')" />
	      </xsl:call-template>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:if>
      </e>
    </xsl:if>
    <xsl:if test="not(contains-token($rewrite/@drop, 'text'))">
      <xsl:if test="text()[normalize-space(.)]">
	<t name="{local-name()}">
	  <xsl:if test="namespace-uri(.)">
	    <xsl:attribute name="ns"
	      select="accumulator-before('namespaces')(namespace-uri(.))" />
	  </xsl:if>
	  <xsl:choose>
	    <xsl:when test=".. instance of document-node()">
	      <!--* root element has text in it, unusual but allowed *-->
	      <xsl:attribute name="parent-name" select=" '#root' " />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:call-template name="make-parent-attributes">
		<xsl:with-param name="node" select=".." />
		<xsl:with-param name="nsmap" select="accumulator-after('namespaces')" />
	      </xsl:call-template>
	    </xsl:otherwise>
	  </xsl:choose>
	</t>
      </xsl:if>
    </xsl:if>
    <xsl:if test="not(contains-token($rewrite/@drop, 'attributes'))">
      <xsl:apply-templates select="@*" />
    </xsl:if>
    <xsl:if test="not(contains-token($rewrite/@drop, 'children'))">
      <!--* search for more elements inside this element: *-->
      <xsl:apply-templates select="*" />
    </xsl:if>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Find a <xd:i>rewrite</xd:i> element that matches a
      given attribute node, or returns () if it  doesn't find
      one (for example, no attribute mapping is in effect)</xd:p>
    </xd:desc>
    <xd:param name="attnode"/>
  </xd:doc>
  <xsl:function name="dc:get-rewrite" as="element(rewrite)?">
    <xsl:param name="node" as="node()" />

    <!--* rewrite ns="*" name="id" drop="value" *-->

    <xsl:variable name="node-ns" select="xs:string(namespace-uri($node))" as="xs:string" />
    <!--* match a rewrite element if
        * there's no namespace name for the input attribute,
	* and none is requested,
	* or,
	* there's a namespace name for the input attribute,
	* and the same one was requested
	*
	* A missing ns attribute on a rewrite element will only match
	* attributes with no namespace URI.
	*-->
    <xsl:sequence
      select="$freqx-control-doc/attribute-overrides/rewrite[
	  (($node-ns ne '') and ((@ns = '*') or (@ns = $node-ns)))
	  or
	  (($node-ns eq '') and (not(@ns) or (@ns = '*') or (@ns = $node-ns)))
	][
	  (: there is always a name for $node :)
	  (not(@name) or (@name eq '*') or (@name eq local-name($node)))
	][
	  if (@element-namespace)
	  then
	    (: for an attribute, match the namespace of the element
	     : that bears the attribute.
	     :)
	    if ($node instance of attribute(*))
	    then @element-namespace = $node/../namespace-uri()
	    else
	      (: for an element, it's same as @ns i suppose :)
	      if ($node instance of element(*))
	      then @element-namespace = $node/namespace-uri()
	      else false() (: not an element or attribute :)
	  else true()
	][
	  if (@parent-namespace)
	  then
	    (: for an attribute, the same as element-namespace :)
	    if ($node instance of attribute(*))
	    then @parent-namespace = $node/../namespace-uri()
	    else (
	      (: for an element, it's the parent's namespace;
	       : is this really useful?
	       :)
	      if ($node instance of element(*))
	      then @parent-namespace = $node/../namespace-uri()
	      else false() (: not an element or attribute :)
	    )
	  else true()
	][
	  (: if there is more than one match take the first :)
	1
	]
      " />
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Convert a sequence of e or t or at elements into
      zero or more count elements representing how many e (or  t)
      elements were seen in the input.</xd:p>
    </xd:desc>
    <xd:param name="data"/>
    <xd:param name="what"/>
  </xd:doc>
  <xsl:function name="dc:count-elements" as="element(count)*">
    <xsl:param name="data" as="element(*)*" />
    <xsl:param name="what" as="xs:string" />

    <!--* Treat (element-name, namespace-uri) as a single element
	* name, and count how many there were of each.
	* We do not here track parent elements.
        *-->
    <xsl:for-each-group select="$data[local-name() = $what]" group-by="@name || ' ' || @ns">
      <xsl:sort select="count(current-group())" data-type="number" order="descending" />
      <xsl:variable name="first" as="element(*)" select="current-group()[1]" />
      <!--* The status=new attribute is so that later we can merge
          * these counts from the current (new) document with the
	  * existing counts and tell them apart.
	  *-->
      <count name="{$first/@name}" status="new">
        <xsl:if test="$first/@ns">
	  <xsl:attribute name="ns" select="$first/@ns" />
	</xsl:if>
	<xsl:value-of select="count(current-group())" />
      </count>
    </xsl:for-each-group>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>
	Convert a sequence of e elements (or $what elements) into
	a sequence of count elements representing how many of each element
	type (as detmerined by attributes on the data elements) have been
	seen. This sequence can then be merged into the counts so far by
	the caller, using a merge function.
      </xd:p>
    </xd:desc>
    <xd:param name="data">
      <xd:p>The data paramater is the unsorted e (or $what) elements.</xd:p>
    </xd:param>
    <xd:param name="what">
      <xd:p>The what parameter is the name of the element to aggregate.</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:function name="dc:count-parent-elements" as="element(count)*">
    <xsl:param name="data" as="element(*)*" />
    <xsl:param name="what" as="xs:string" />

    <!--* Treat (element-name, namespace-uri, parent) as a single element
	* name, and count how many there were of each.
	* parent here is (parent-name, parent-ns)
        *-->
    <xsl:for-each-group select="$data[local-name() = $what]"
      group-by="@name || ' ' || @ns || ' ' || @parent-name || ' ' || @parent-ns">
      <xsl:variable name="first" as="element(*)" select="current-group()[1]" />
      <count>
	<xsl:copy-of select="$first/(@name, @ns, @parent-name, @parent-ns)" />
	<xsl:value-of select="count(current-group())" />
      </count>
    </xsl:for-each-group>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Convert a sequence of at elements (possibly interspersed with
      other elements) into a sequence of count elements showing how
      many attributes with each different name were found. These
      count elements can be merged with other count elements by
      the caller using a merge function.</xd:p>
    </xd:desc>
    <xd:param name="data">
      <xd:p>The data paramater is the unsorted "at" elements.</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:function name="dc:count-attribute-values" as="element(count)*">
    <xsl:param name="data" as="element(*)*" />

    <xsl:for-each-group select="$data[self::at]"
      group-by="@name || ' ' || @ns || ' ' ||
                @parent-name || ' ' || @parent-ns ||
		' ' || @value
		">
      <xsl:variable name="first" as="element(*)" select="current-group()[1]" />
      <count status="new" name="{$first/@name}"
	parent-name="{$first/@parent-name}">
        <xsl:if test="$first/@ns">
	  <xsl:attribute name="ns" select="$first/@ns" />
	</xsl:if>
        <xsl:if test="$first/@value">
	  <xsl:attribute name="value" select="xs:string($first/@value)" />
	</xsl:if>
        <xsl:if test="$first/@parent-ns">
	  <xsl:attribute name="parent-ns" select="$first/@parent-ns" />
	</xsl:if>
	<xsl:value-of select="count(current-group())" />
      </count>
    </xsl:for-each-group>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>merge-elements() is the primary merging function.
      it takes a sequence of count elements representing what
      has already been seen, an unsorted sequence ($data) of
      elements representing what was seen in the most recent
      document to be processed, and the name of the elements
      to combine. It produces a sequence or count elements
      representing how many of $what have been seen, updating
      the <xd:i>ndocs</xd:i> attributes on the count elements
      to relect one more input document.</xd:p>
    </xd:desc>
    <xd:param name="counts-so-far"/>
    <xd:param name="data"/>
    <xd:param name="what"/>
  </xd:doc>
  <xsl:function name="dc:merge-elements" as="element(count)*">
    <xsl:param name="counts-so-far" as="element(count)*" />
      <!--* counts-so-far is from  previous iterations *-->
    <xsl:param name="data" as="element(*)*" />
      <!--* $data is new data from the document we just processed *-->
    <xsl:param name="what" as="xs:string" />
      <!--* the element we should count *-->

    <!--* take a set (OK, sequence) of count elements and a
        * sequence of e elements representing occurrences,
	* turn the elements into counts,
	* and returns a sequence of counts that includes the
	* new count elements
	*-->

    <!--* if processing the input failed, we don't need to
        * count the elements found (although if we do, we won't
	* find any):
	*-->
    <xsl:variable name="fail"
      select="empty($data) or exists($data//fail)" />

    <xsl:variable name="data-counts" as="element(count)*"
      select="if ($fail)
              then ()
	      else dc:count-elements($data, $what)" />

    <xsl:for-each-group select="($counts-so-far , $data-counts)"
	group-by="@name || ' ' || @ns || ' ' || @value">
	<!--* Note: the incoming count elements have other attributes,
	    * such as status="new" and maybe parent=..., that we do
	    * not want to group by here
	    *-->

      <xsl:variable name="first" as="element(count)" select="current-group()[1]" />
      <!--* make a new count element to represent this combination
          * of a bunch of "e" or "at" or whatever, combined with
	  * zero or one "count" elements representing the "e" or "at"
	  * elements we saw in previous documents, either this run or
	  * loaded from before.
	  *
	  * @ndocs represent the number of documents in which this
	  * particular @name || @ns || @value combination occurred.
	  * We add 1 for the currently-processed document if it went
	  * OK. Since  count-elements adds a status="new" attribute to
	  * the count elements it returns for the current document,
	  * we can tell if the grouping combination occurred in the
	  * current document or was in previously processed data.
	  *
	  * Note: count elements, including $first, will have extra
	  * attributes we don't want here, including status=new, and
	  * also parent name and namespace; we use those in the
	  * merge-element-parents function.
	  *-->
      <count name="{$first/@name}"
             ndocs="{ xs:integer(
		 (if (current-group()/@status = 'new') then 1 else 0)
		 + sum( (current-group()[not(@status)]/@ndocs ! xs:integer(.)))
	       )
             }"
      >
	<xsl:if test="$first/@ns">
          <xsl:attribute name="ns" select="$first/@ns" />
        </xsl:if>
        <!--* the content is the number of such elements in each set,
            * so we add up the element content:
            *-->
        <xsl:value-of select="sum(current-group()/text() ! xs:integer(.))" />
      </count>
    </xsl:for-each-group>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>group-element-parents is a function that
      is  used to generate a grouping key.  There should be one
      of these for each different grouping key, so that the various
      counting and merging functions can be combined, but that
      work has not yet been done.</xd:p>
    </xd:desc>
    <xd:param name="count"/>
  </xd:doc>
  <xsl:function name="dc:group-element-parents" as="xs:string">
    <xsl:param name="count" as="element(count)" />
    <xsl:for-each select="$count"><!--* set the context item *-->
      <xsl:sequence select="
            @name || ' ' || @ns || @parent-name || ' ' || @parent-ns
          " />
    </xsl:for-each>
  </xsl:function>


  <xd:doc>
    <xd:desc>
      <xd:p>merge-element-parents takes raw data from processing
      the input (e elements) and groups and counts them, merging
      the resulting count elements with count elements already
      produced (either from an earlier run or this run, or a mix).</xd:p>
    </xd:desc>
    <xd:param name="counts-so-far">
      <xd:p><xd:i>count</xd:i> elements produced so far</xd:p>
    </xd:param>
    <xd:param name="data">
      <xd:p>The incoming $what elements ("e" elements in practice)</xd:p>
    </xd:param>
    <xd:param name="what">
      <xd:p>The name of the elements we want to count</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:function name="dc:merge-element-parents" as="element(count)*">
    <!--* Like merge-elements but with a different merging criterion.
        * If the merge criterion was passed as a function, we could
	* have less repetition. TODO
	*-->
    <xsl:param name="counts-so-far" as="element(count)*" />
    <xsl:param name="data" as="element(*)*" />
    <xsl:param name="what" as="xs:string" />

    <!--* if processing the input failed then we don't need to
        * count the elements found (although if we do, we won't
        * find any):
        *-->
    <xsl:variable name="fail" select="empty($data) or exists($data//fail)" />

    <xsl:variable name="data-counts" as="element(count)*"
      select="if ($fail)
	      then ()
	      else dc:count-parent-elements($data, $what)" />

    <xsl:for-each-group select="($counts-so-far, $data-counts)"
	group-by="dc:group-element-parents(.)">

      <xsl:variable name="first" as="element(count)" select="current-group()[1]" />
      <count status="new">
	<xsl:copy-of select="$first/(@name, @ns, @parent-name, @parent-ns)" />
	 <!--* the content is the number of such elements in each set,
	     * so we add up the element content:
	     *-->
	<xsl:text>{ sum(current-group()/text() ! xs:integer(.)) }</xsl:text>
      </count>
    </xsl:for-each-group>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Merge newly-found occurrence data (e.g. e, t, and at elements)
      into a set of already-totalled counts.</xd:p>
      <xd:p>This is very similar to merge-element-values and
      probably there should be only one function with a function
      parameter that does the grouping and another to construct the
      new count elements.</xd:p>
    </xd:desc>
    <xd:param name="counts-so-far">
      <xd:p>A sequence of 0 or more count elements representing
      attribue name and value combinations already processed</xd:p>
    </xd:param>
    <xd:param name="data">
      <xd:p>The unsorted data, most likely  just a sequence of zero or
      more at elements.</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:function name="dc:merge-attribute-values" as="element(count)*">
    <xsl:param name="counts-so-far" as="element(count)*" />
    <xsl:param name="data" as="element(*)*" />

    <xsl:variable name="data-counts"
      select="if (empty($data)) then () else dc:count-attribute-values($data)" as="element(count)*" />

    <xsl:for-each-group select="($counts-so-far , $data-counts)"
	  group-by="
	    @name || ' ' || @ns ||
	    @parent-name || ' ' || @parent-ns ||
	    ' ' || @value
	    ">
      <xsl:variable name="first" as="element(count)" select="current-group()[1]" />
      <!--* note: avoid copying namespace nodes associated with the value,
          * e.g. with Schema-validated QNames, by casting to string
	  *-->
      <count name="{$first/@name}"
	     parent-name="{$first/@parent-name}"
	     value="{xs:string($first/@value)}"
             ndocs="{
	       xs:integer(
		 (if (current-group()/@status = 'new') then 1 else 0)
		 + sum( (current-group()[not(@status)]/@ndocs ! xs:integer(.)))
	       )
             }"
	     >
        <xsl:if test="$first/@ns">
	  <xsl:attribute name="ns" select="$first/@ns" />
	</xsl:if>
        <xsl:if test="$first/@parent-ns">
	  <xsl:attribute name="parent-ns" select="$first/@parent-ns" />
	</xsl:if>
	<!--* the content is the number of such elements in each set,
	    * so we add up the element content:
	    *-->
	<xsl:value-of select="sum(current-group()/text() ! xs:integer(.))" />
      </count>
    </xsl:for-each-group>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Match attribute nodes and turn them into
      <xd:i>at</xd:i> elements so we can count them later.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="@*">
    <xsl:variable name="rewrite" select="dc:get-rewrite(.)" />

    <!--* rewrite ns="*" name="ignore-me" drop="all" *-->
    <xsl:if test="not( contains-token($rewrite/@drop, 'all') )">
      <at name="{local-name(.)}">
        <xsl:if test="not(contains-token($rewrite/@drop, 'value'))">
	  <!--* use rewrite value="fixed" if given: *-->
	  <xsl:attribute name="value" select="($rewrite/@value, .)[1]" />
	</xsl:if>
	<xsl:if test="namespace-uri(.)">
	  <xsl:if test="not(contains-token($rewrite/@drop, 'namespace'))">
	    <xsl:attribute name="ns" select="namespace-uri(.)" />
	  </xsl:if>
	</xsl:if>
	<xsl:if test="not(contains-token($rewrite/@drop, 'parent'))">
	  <!--* ".." must be an element *-->
	  <xsl:for-each select="..">
	    <xsl:call-template name="make-parent-attributes">
	      <xsl:with-param name="node" select="." />
	      <xsl:with-param name="nsmap" select="accumulator-after('namespaces')" />
	    </xsl:call-template>
	  </xsl:for-each>
	</xsl:if>
      </at>
    </xsl:if>
  </xsl:template>

  <xd:doc>
    <xd:p>dc:none(s) returns true if s is empty (that is, the
    empty sequence or the empty string) or has the value
    '#none'; this is so that you can reliably pass a stylesheet parameter
    to the XSLT engine whose value can mean 'none' in the context of
    a URI; this is OK since # introduces a fragment identifier in a URI
    and is not part of the filename. For a literal filename of '#none'
    you'd have to use URI encoding: %23none.</xd:p>
  </xd:doc>
  <xsl:function name="dc:none" as="xs:boolean">
    <xsl:param name="input" as="xs:string?" />

    <xsl:sequence select="
	empty($input) or $input eq '' or $input eq '#none'
    " />
  </xsl:function>

  <xd:doc>
    <xd:p>dc:v() takes an attribute or an empty sequence
    and returns a string, empty in the case of the empty
    sequence. This way we can generate values in comma-separated
    value files without worrying about empty sequences getting
    thrown away.</xd:p>
  </xd:doc>
  <xsl:function name="dc:v" as="xs:string">
    <xsl:param name="input" as="attribute(*)?" />
    <xsl:sequence select="( xs:string($input), '' )[1] "/>
  </xsl:function>

  <!--**************************************************
      *
      * The reports
      *
      *-->

  <xd:doc>
    <xd:desc>
      <xd:p>Generate a comma=separated-values report</xd:p>
    </xd:desc>
    <xd:param name="counts">The freqx element containing the aggregated count elements</xd:param>
  </xd:doc>
  <xsl:template name="make-csv-report">
    <xsl:param name="counts" as="element(freqx)" />

    <!--* Make a comma-separated-value report that you can load into
        * libreoffice or Lotus 1-2-3 for further analaysis.
	*
	* There are two ways of working:
	* (1) make separate files for each of several reports, or
	* (2) make a single combined report
	*
	* Which separate reports you get depends on the value of
	* $csv-output-separate-files
	* elements element-parents
	* attributes attribute-values
	* text-elements
	* docs
	* errors
	* full (make the full result file)
	* The value all is taken to mean every individual report;
	* use "full all" to get everything.
	* Use #none to get only the full report in $csv-output-uri
	* Set _that_ in turn to #none to get no reports at all.
	* It's easier to use than to describe. I hope.
	*-->

    <xsl:variable name="csv-makers" as="map(*)"
      select="
	  (: This data structure drives the various different
	   : comma-separated-value (CSV) reports.
	   :)
	map {
	  'elements' : map {
	    'what' : function($counts) { $counts/elements/* },
	    'headings' : 'Element,NS,Nocc,NDocs',
	    'attributes' : function($count as element(count)) {
	      (
		dc:v($count/@name),dc:v($count/@ns),xs:string($count),dc:v($count/@ndocs)
	      )
	    }
	  },
	  'element-parents' : map {
	    'what' : function($counts) { $counts/elements-parents/* },
	    'headings' : 'Element,NS,Parent,Parent NS,Nocc,NDocs',
	    'attributes' : function($count as element(count)) {
	      (
		dc:v($count/@name),dc:v($count/@ns),dc:v($count/@parent-name),dc:v($count/@parent-ns),xs:string($count),dc:v($count/@ndocs)
	      )
	    }
	  },
	  'attributes' : map {
	    'what' : function($counts) { $counts/attributes/* },
	    'headings' : 'Attribute,NS,Parent,Parent NS,Nocc,NDocs',
	    'attributes' : function($count as element(count)) {
	      (
		dc:v($count/@name),dc:v($count/@ns),dc:v($count/@parent-name),dc:v($count/@parent-ns),xs:string($count),dc:v($count/@ndocs)
	      )
	    }
	  },
	  'attribute-values' : map {
	    'what' : function($counts) { $counts/attribute-values/* },
	    'headings' : 'Attribute,NS,Parent,Parent NS,Value,Nocc,NDocs',
	    'attributes' : function($count as element(count)) {
	      $count/(
		dc:v($count/@name),dc:v($count/@ns),dc:v($count/@parent-name),dc:v($count/@parent-ns),dc:v($count/@value),xs:string($count),dc:v($count/@ndocs)
	      )
	    }
	  },
	  'text-elements' : map {
	    'what' : function($counts) { $counts/attributes/* },
	    'headings' : 'Text Element,NS,Parent,Parent NS,Nocc,NDocs',
	    'attributes' : function($count as element(count)) {
	      (
		dc:v($count/@name),dc:v($count/@ns),dc:v($count/@parent-name),dc:v($count/@parent-ns),dc:v($count/@value),xs:string($count),dc:v($count/@ndocs)
	      )
	    }
	  },
	  'docs' : map {
	    'what' : function($counts) { $counts/docs/* },
	    'headings' : 'DocNo,URI',
	    'attributes' : function($doc as element(doc)) as xs:string* {
		string($doc/position()),
		normalize-space(replace($doc/@name, '([\\,])', '\\$1'))
	    }
	  },
	  (: If you explicitly ask for error details you get them all here,
	   : even if they are otherwise suppressed.
	   : CAUTION, Microsoft Excel has a maximum number of rows in
	   : a spreadsheet, i hear.
	   :)
	  'errors' : map {
	    'what' : function($counts) { $counts/errors/* },
	    'headings' : 'DocNo,URI,Errors',
	    'attributes' : function($fail as element(fail)) as xs:string* {
		string($fail/position()),
		normalize-space(replace($fail/@doc, '([\\,])', '\\$1')),
		normalize-space(replace($fail/@reason, '([\\,])', '\\$1'))
	    }
	  }
	}
      " />

    <!--* Before we start, let's check out input data. We check that
	* we know how to make every requested report before writing any:
        *-->
    <xsl:if test="not(dc:none($csv-output-uri))
              and not(dc:none($csv-output-separate-files))">
      <xsl:if test="contains-token($csv-output-separate-files, 'help')">
	<xsl:message>Possible tokens for the csv-output-separate-files parameter, space separated are:{ $NL }{
	  string-join(map:keys($csv-makers), ' ')
	}</xsl:message>
	<xsl:message terminate="yes">[CSV files not written]</xsl:message>
      </xsl:if>
      <xsl:for-each select="tokenize($csv-output-separate-files)">
	<xsl:if test="not(map:contains($csv-makers, lower-case(.)))">
	  <xsl:message terminate="yes">Unkown value in $csv-output-separate-files {.}</xsl:message>
	</xsl:if>
      </xsl:for-each>
    </xsl:if>

    <xsl:variable name="all" as="xs:boolean"
      select="contains-token($csv-output-separate-files, 'all')" />

    <!--* The reports will append the report name to the file,
        * before any .csv suffix, and then add a csv suffix.
	*-->
    <xsl:variable name="output-stub" as="xs:string"
      select="replace($csv-output-uri, '\.[cC][sS][vV]$', '')" />

    <xsl:if test="not(dc:none($csv-output-separate-files))">
      <!--* so we want individual files *-->
      <xsl:for-each select="map:keys($csv-makers)">
	<!--* if we want this one, make it: *-->
	<xsl:if test="$all or contains-token($csv-output-separate-files, .)">
	  <xsl:result-document method="text" href="{$output-stub}-{.}.csv">
	    <xsl:variable name="map" select="$csv-makers(.)" as="map(*)" />

	    <xsl:text>{ $map?headings || $NL }</xsl:text>
	    <xsl:for-each select="$map?what($counts)">
	      <!--* so . is a single count element for one row of report *-->
	      <xsl:sort data-type="number" order="descending"
	      select="if (. castable as xs:integer) then xs:integer(.) else 0" />
	      <xsl:sort select="@name || ' ' || @ns" />
	      <xsl:text>{string-join( $map?attributes(.), ',')}{$NL}</xsl:text>
	    </xsl:for-each>
	  </xsl:result-document>
	  <p>CSV output for {.} written to {$output-stub}-{.}.csv</p>
	</xsl:if>
      </xsl:for-each>
    </xsl:if>

    <!--* if we want a combined report, make one: *-->
    <xsl:if test="dc:none($csv-output-separate-files) or
                  contains-token($csv-output-separate-files, 'full')">
      <xsl:result-document href="{$csv-output-uri}" method="text">
	<xsl:for-each select="map:keys($csv-makers)">
	  <xsl:variable name="map" select="$csv-makers(.)" as="map(*)" />

	  <xsl:text>{ $map?headings || $NL }</xsl:text>
	  <xsl:for-each select="$map?what($counts)">
	    <!--* so . is a single count element for one row of report *-->
	    <xsl:sort data-type="number" order="descending"
	      select="if (. castable as xs:integer) then xs:integer(.) else 0" />
	    <xsl:sort select="@name || ' ' || @ns" />
	    <xsl:text>{ string-join($map?attributes(.), ',') }{$NL}</xsl:text>
	  </xsl:for-each>
	  <!--* separator *-->
	  <xsl:text>,,,,,,{$NL}</xsl:text>
	</xsl:for-each>
      </xsl:result-document>
      <p>CSV output written to {$csv-output-uri}</p>
    </xsl:if>
  </xsl:template>

  <xsl:variable name="namespace-prefixes" as="map(xs:string, xs:string)"
    select='map {
      "http://www.w3.org/XML/1998/namespace" : "xml",
      "http://www.w3.org/2000/xmlns/" : "xmlns", (: unlikely! :)
      "http://www.w3.org/1999/xhtml" : "xhtml",
      "http://www.w3.org/1999/xlink" : "xlink",
      "http://www.w3.org/2001/XInclude" : "xi", (: XInclude :)
      "http://www.w3.org/1999/XSL/Format" : "fo", (: XSL-FO :)
      "http://www.w3.org/1999/XSL/Transform" : "xsl", (: XSLT :)
      "http://www.w3.org/2001/XMLSchema" : "xs", (: XML Schema :)
      "http://www.w3.org/2001/XMLSchema-datatypes" : "dt",
      "http://www.w3.org/2001/XMLSchema-instance" : "xsi",
      "http://www.w3.org/2000/01/rdf-schema#" : "rdfs",
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#" : "rdf",
      "http://www.w3.org/2000/svg" : "svg",
      "http://creativecommons.org/ns#" : "cc", (: creative commons :)
      "http://purl.org/dc/elements/1.1/" : "dc", (: dublin core :)
      "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" : "sodipodi", (: sodipodi was the original name for what is now Inkscape :)
      "http://www.inkscape.org/namespaces/inkscape" : "inkscape",
      "http://www.w3.org/2001/SMIL20/" : "smil",
      "http://www.w3.org/1998/Math/MathML" : "mml", (: mathml :)
      "http://docbook.org/ns/docbook" : "db", (: docbook :)
      "http://dita.oasis-open.org/architecture/2005/" : "dita",
      "http://jats.nlm.nih.gov" : "jats",
      "http://www.niso.org/schemas/ali/1.0" : "ali", (: NISO access license and indicators :)
      "http://schemas.xmlsoap.org/soap/envelope/" : "soap",
      "http://schemas.xmlsoap.org/wsdl/" : "wsdl",
      "http://schemas.xmlsoap.org/wsdl/soap/" : "wsoap", (: another soap ns :)
      "http://www.w3.org/2005/05/xmlmime" : "xmime",
      "http://www.oxygenxml.com/ns/doc/xsl" : "xd", (: Oxygen docs :)
      "http://www.w3.org/2005/xqt-errors" : "err",
      "http://www.w3.org/2005/xpath-functions/map" : "map",
      "http://www.w3.org/2005/xpath-functions/array" : "array",

      "http://openoffice.org/2009/office" : "oooffice",
      "http://www.niso.org/standards/z39-96/ns/oasis-exchange/table" : "oasis",
      "http://www.xbrl.org/2003/linkbase" : "link", (: xbrl :)
      "http://www.xbrl.org/2003/instance" : "xbrli",
      "http://www.xbrl.org/2003/XLink" : "xbrl-xl",  (: why?? :)
      "http://www.xbrl.org/2003/iso4217" : "iso-xbrl"
    }' />

  <xd:doc>
    <xd:p>dc:display-name() generates a string name for a
    (local-name, namespace-URI) pair, using a table of
    well-known namespace names to generate common prefixes.</xd:p>
    <xd:p>We can't in general use the prefixes from the actual original
    documents; we have not preserved namespace nodes when making
    count elments, and even if we did, there's no guarantee XSLT would
    generate what we wanted here, because the same namespace URI might
    have different prefixes bound to it in different documents (or
    even the same document, e.g. for XSLT that generates XSLT).</xd:p>
    <xd:param name="name">The local-name() part of the name, an NCNAME;</xd:param>
    <xd:param name="ns">The namespace name, a URI normally, or empty</xd:param>
  </xd:doc>
  <xsl:function name="dc:display-name" as="xs:string">
    <xsl:param name="name" as="xs:string" />
    <xsl:param name="ns" as="xs:string?" />

    <!--* sanity; the order of the parameters is maybe confusing here,
	* but elsewhere we have @name before @ns, and @ns is optional.
        *-->
    <xsl:if test="empty($name) or ($name eq '')">
      <xsl:message terminate="yes">dc:display-name({$name}, {$ns}): first argument must be the local-name of an element or attribute, not empty</xsl:message>
    </xsl:if>
    <xsl:if test="not(matches($name, '^(#root|(\i\c*))$')) or contains($name, ':')">
      <xsl:message terminate="yes">dc:display-name({$name}, {$ns}): first argument must be the local-name of an element or attribute, and cannot contain a colon or  non-name characters.</xsl:message>
    </xsl:if>

    <!--* see if we can find a prefix for $ns *-->
    <xsl:variable name="prefix" as="xs:string?"
      select="
	if (empty($ns) or ($ns eq ''))
	then ''
	else if ($namespace-prefixes($ns))
	  then ($namespace-prefixes($ns) || ':')
	else '*:'
      " />

    <xsl:sequence select="$prefix || $name" />
  </xsl:function>

  <xd:doc>
    <xd:p>dc:full-name() generates a full name for a
    (local-name, namespace-URI) pair, including prefix where
    known.</xd:p>
    <xd:param name="name">The local-name() part of the name, an NCNAME;</xd:param>
    <xd:param name="ns">The namespace name, a URI normally, or empty</xd:param>
  </xd:doc>
  <xsl:function name="dc:full-name" as="xs:string">
    <xsl:param name="name" as="xs:string" />
    <xsl:param name="ns" as="xs:string?" />

    <!--* sanity; the order of the parameters is maybe confusing here,
	* but elsewhere we have @name before @ns, and @ns is optional.
        *-->
    <xsl:if test="empty($name) or ($name eq '')">
      <xsl:message terminate="yes">dc:full-name({$name}, {$ns}): first argument must be the local-name of an element or attribute, not empty</xsl:message>
    </xsl:if>
    <xsl:if test="not(matches($name, '^\i\c*$')) or contains($name, ':')">
      <xsl:message terminate="yes">dc:full-name({$name}, {$ns}): first argument must be the local-name of an element or attribute, and cannot contain a colon or  non-name characters.</xsl:message>
    </xsl:if>

    <!--* see if we can find a prefix for $ns *-->
    <xsl:variable name="namespace-name" as="xs:string"
      select="
        if (empty($ns) or ($ns eq ''))
	then ''
	else $ns
	" />

    <xsl:variable name="prefix" as="xs:string?"
      select="
	if ($namespace-name eq '') then ''
	else if ($namespace-prefixes($namespace-name))
	  then ($namespace-prefixes($namespace-name) || ':')
	else '*:'
      " />

    <xsl:sequence select="$prefix || $name ||
      (
	if ($namespace-name eq '') then ''
	else (' [' || $namespace-name || ']')
      )
    " />
  </xsl:function>

  <xd:doc>
    <xd:p>The dc:percent() function takes a non-negative value and a maximum
    value, expresses the value as a percentage of the maximum, and generates
    CSS class tokens that can be used in the HTML report to style values
    differently depending on their value as a percentage.</xd:p>
    <xd:param name="input">The input value, which must be in
    the range 0..$max-value inclusive;</xd:param>
    <xd:param name="max-value">the largest value in the data set.</xd:param>
  </xd:doc>
  <xsl:function name="dc:percent-class" as="xs:string">
    <xsl:param name="input" as="xs:double" />
    <xsl:param name="max-value" as="xs:double" />

    <!--* First some sanity checking.  *-->

    <!--* allow input to be slightly greater than the max value,
        * to deal with rounding errors:
	*-->
    <xsl:variable name="slop" as="xs:double" select="0.01" />

    <xsl:if test="$input gt ($max-value + $slop)">
      <xsl:message terminate="yes">dc:percent({$input}, {$max-value}): error: first argument is greater than second, which should be maximum.</xsl:message>
    </xsl:if>

    <xsl:if test="$input lt 0">
      <!--* no $slop tolerance for negatives: *-->
      <xsl:message terminate="yes">dc:percent({$input}, {$max-value}): error: both arguments must be greater than or equal to zero</xsl:message>
    </xsl:if>

    <!--* Now work out the percentage, clamped to an upper bound of 100%
        * even where we allow some slop.
        *-->
    <xsl:variable name="percent" as="xs:double"
      select="
        let $value := 100.0 * ($input div $max-value)
	return if ($value gt 100.0) then 100.0 else $value
	" />

    <!--* Now, choose a class so we can hang styles off the result.
        * To make it easier we will return
	* percent-ru-NNN - the integer percentage, rounded up
	* percent-rd-NNN - the integer percentage, rounded down,
	* quartile (00, 25, 50, 75)
	*-->
    <xsl:sequence select="string-join(
      (
	'percent-ru-' ||
	  format-integer(xs:integer(ceiling($percent)), '777'),
	'percent-rd-' ||
	  format-integer(xs:integer(floor($percent)), '777'),
	'quartile-' ||
	  format-integer(xs:integer(25 * floor($percent div 25)), '777')
      ),
      ' ')"/>

  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Write out an HTML 5 report; the input parameter is the
      merged data for the report.</xd:p>
    </xd:desc>
    <xd:param name="counts">The freqx element containing the aggregated count elements</xd:param>
  </xd:doc>
  <xsl:template name="make-html-report">
    <xsl:param name="counts" as="element(freqx)" />

    <xsl:variable name="element-counts" as="element(count)*" select="$counts/elements/*" />
    <xsl:variable name="root-element-counts" as="element(count)*" select="$counts/element-parents/*[@parent-name = '#root']" />
    <xsl:variable name="element-parent-counts" as="element(count)*" select="$counts/element-parents/*" />
    <xsl:variable name="attribute-counts" as="element(count)*" select="$counts/attributes/*" />
    <xsl:variable name="attribute-value-counts" as="element(count)*" select="$counts/attribute-values/*" />

    <!--* in the bar charts that we produce we must allow enough space
        * for the widest number. If the digits are all the same width,
	* we can use the CSS unit ch for the width of the zero.  If they are not all the same
	* width, 0 is widest in ranging (lined) numerals, and not too far off average
	* in old-style (non-ranging) figures. We could force table  figures if available,
	* but that seems overly controlling.
	*
	* We'll find the largest count, turn it into a string, and count the digits:
	*-->
    <xsl:variable name="max-digits-in-counts" as="xs:integer"
       select="string-length(xs:string(max(
	(3, (: always leave at least 3 digits of space so reports don't vary too much :)
	  ( $element-counts ! xs:integer(.) ),
	  ( $attribute-counts ! xs:integer(.) ) ,
	  ( $attribute-value-counts ! xs:integer(.) )
	)
       )))" />


    <xsl:result-document href="{$html-output-uri}" method="html" html-version="5">
      <html>
	<head>
	  <title>FreqX Report</title>
	  <link rel="stylesheet" href="{$html-output-css-uri}" />
	  <style>
	    :root {'{'}
	      --maxdigits: { $max-digits-in-counts };
	    {'}'}
	  </style>
	</head>
	<body>
	  <h1>FreqX Report of Markup Frequencies</h1>

	  <section class="summary">
	    <h2>Summary</h2>
	    <p>FreqX report from {
	      format-dateTime(current-dateTime(),
		(:  3.58pm on Tuesday, 31st December 1938 :)
		"[h].[m01] [Pn] on [FNn], [D1o] [MNn] [Y0001]"
	      )
	    }.</p>
	    <details>
	      <summary>Configuration...</summary>
	      <ol>
	      <li><span class="name">collection</span>: {$collection}</li>
	      <li><span class="name">collection-uri</span>: {$collection-uri}</li>
	      <li><span class="name">oxygen-project-file</span>: {$oxygen-project-file}</li>
	      <li><span class="name">pattern</span>: {$pattern}</li>
	      <li><span class="name">recurse</span>: {$recurse}</li>
	      <li><span class="name">max-docs</span>: {$max-docs}</li>
	      <li><span class="name">max-file-bytes</span>: {$max-file-bytes}</li>
	      <li><span class="name">include-error-details</span>: {$include-error-details}</li>
	      <li><span class="name">merge-counts-from</span>: {$merge-counts-from}</li>
	      <li><span class="name">freqx-control-file</span>: {$freqx-control-file}</li>
	      <li><span class="name">xml-output-uri</span>: {$xml-output-uri}</li>
	      <li><span class="name">write-collection-file-uri</span>: {$write-collection-file-uri}</li>
	      <li><span class="name">csv-output-uri</span>: {$csv-output-uri}</li>
	      <li><span class="name">csv-output-separate-files</span>: {$csv-output-separate-files}</li>
	      <li><span class="name">html-output-uri</span>: {$html-output-uri}</li>
	      <li><span class="name">html-output-css-uri</span>: {$html-output-css-uri}</li>
	      </ol>
	    </details>
	    <p>Total documents: {
	      count($counts/docs/*) + count($counts/errors/fail)
	    }.</p>
	    <p>Processed successfully: {count($counts/docs/*)}.</p>
	    <p>Documents with errors, not included: {count($counts/errors/fail)}.</p>
	    <xsl:if test="count($counts/errors/fail) and not($include-error-details eq 'no')">
	      <details>
		<summary>Details of errors...</summary>
		<xsl:if test="
		  count($counts/errors/fail) gt (
		      if (matches($include-error-details, '^\d+$'))
		      then xs:integer($include-error-details)
		      else 0
		    )
		    ">
		  <p>Reporting only on the first {$include-error-details}
		  document{ if ($include-error-details eq '1') then '' else 's'}:</p>
		</xsl:if>

		<ol>
		  <xsl:for-each select="$counts/errors/fail[
		    if (matches($include-error-details, '^\d+$')) then
		    position() le xs:integer($include-error-details)
		    else true()
		  ]" >
		    <li>
		      <span class="fn">{@doc}</span> - <span class="failwhy">{@reason}</span>
		    </li>
		  </xsl:for-each>
		</ol>
	      </details>
	    </xsl:if>
	    <p>Total distinct <a href="#elements">elements</a> (ignoring namespaces): {
	      count($element-counts)
	    }.</p>
	    <p>Total distinct <a href="#attributes">attribute</a> names (ignoring namespaces): {
	      count(distinct-values($counts/attributes/*/@name))
	    }.</p>
	  </section>

	  <!--* The individual reports in HTML, with enough in them to generate
	      * bar charts in CSS. I looked at adding pie charts for the top
	      * few values but the nature of the data means that's probably not
	      * very useful.
	      *
	      * What follows are a whole bunch of very similar-looking mappings,
	      * but the details are different, ranging from sorting and goruping
	      * parameters through to content. It would be possible to treat
	      * these in the same way that the CSV report is treated, with a
	      * map of grouping, sorting, content functions, and maybe that would
	      * be better if more reports are added.
	      *
	      * NOTE: we map a percentage to a percent of 99% for the bar chart
	      * width, to avoid risk of overflowing in the browser.
	      *-->
	  <section id="root-elements">
	    <h2><a name="root-elements">Top Level Elements</a></h2>
	    <xsl:variable name="max-root-occ" select="max($root-element-counts)" />
	    <div class="counts root-element-counts element-colours">
	      <xsl:for-each select="$root-element-counts">
		<xsl:sort select="xs:integer(.)" order="descending" />
		<xsl:sort select="@name" />
		<details>
		  <summary>
		    <div class="name">{dc:display-name(@name, @ns)}</div>
		    <div class="freq {
		      dc:percent-class(., $max-root-occ)
		    }"
		    >
		      <xsl:variable name="percent" as="xs:double"
		      select="99 * . div  $max-root-occ" />
		      <div class="value">{.}</div>
		      <div class="bar" style="width: {$percent}%;">&#160;</div>
		    </div>
		  </summary>
		  <p><span class="name">{dc:full-name(@name, @ns)}</span></p>
		</details>
	      </xsl:for-each>
	    </div>
	  </section>

	  <section id="elements">
	    <h2><a name="elements">Elements</a></h2>
	    <xsl:variable name="max-element-occ" select="max($element-counts)" />
	    <div class="counts element-counts element-colours">
	      <xsl:for-each select="$element-counts">
		<xsl:sort data-type="number" order="descending" select="xs:integer(.)" />
		<xsl:sort select="@name || ' ' || @ns" />
		<details>
		  <summary>
		    <div class="name"><a name="e-{@name}"></a>{dc:display-name(@name, @ns)}</div>
		    <div class="freq {
		      dc:percent-class(., $max-element-occ)
		    }">
		      <xsl:variable name="percent" as="xs:double"
			select="99 * . div  $max-element-occ" />
		      <div class="value">{.}</div>
		      <div class="bar" style="width: {$percent}%;">&#160;</div>
		    </div>
		  </summary>

		  <!--* Details: how many docs? *-->
		  <h3>Frequency</h3>
		  <p><span class="name">{dc:full-name(@name, @ns)}</span> was found in {@ndocs} document{if (@ndocs eq '1') then '' else 's'}.</p>
		  <!--* Details: the parents of this element *-->
		  <xsl:variable name="gi" select="@name" as="xs:string" />
		  <h3>Parents of {$gi}:</h3>
		  <ul>
		  <xsl:for-each
		    select="$element-parent-counts[@name eq $gi]">

		    <xsl:sort select="xs:integer(.)" data-type="number" order="descending" />
		    <xsl:sort select="@name" order="ascending" />

		    <li><span class="name"><a href="#e-{@parent-name}">{dc:display-name(@parent-name, @parent-ns)}</a></span> {xs:integer(.)}</li>
		  </xsl:for-each>
		  </ul>

		  <!--* the children of this element *-->
		  <h3>Elements immediately contained in {$gi}</h3>
		  <xsl:variable name="myns" as="xs:string" select=" (@ns, '')[1] " />
		  <xsl:sequence>
		    <xsl:where-populated>
		      <ul>
			<xsl:for-each
			  select="$element-parent-counts[ (@parent-name eq $gi) and ((@parent-ns, '')[1] eq $myns) ]">

			  <xsl:sort select="xs:integer(.)" data-type="number" order="descending" />
			  <xsl:sort select="@name" order="ascending" />

			  <li><span class="name"><a href="#e-{@name}" class="eref">{dc:display-name(@name, @parent-ns)}</a></span> {xs:integer(.)}</li>
			</xsl:for-each>
		      </ul>
		    </xsl:where-populated>
		    <xsl:on-empty>
		      <p>[no element children]</p>
		    </xsl:on-empty>
		  </xsl:sequence>

		  <!--* what attributes were seen on this element? *-->
		  <h3>Attributes found on {$gi}</h3>
		  <xsl:sequence>
		    <xsl:where-populated>
		      <ul>
			<xsl:for-each-group
			  select="$attribute-value-counts[
			      (@parent-name eq $gi)
			      and
			      (($myns eq '') or (@parent-ns = $myns))
			  ]"
			  group-by="@name || ' ' || @ns">

			  <xsl:sort select="@name || ' ' || @ns" order="ascending" />

			  <li><span class="name"><a href="#a-{@name}">@{
			  dc:display-name(@name, @ns)
			  }</a></span> {sum(current-group())}</li>

			</xsl:for-each-group>
		      </ul>
		      </xsl:where-populated>
		    <xsl:on-empty>
		      <p>[no attributes]</p>
		    </xsl:on-empty>
		  </xsl:sequence>

		</details>
	      </xsl:for-each>
	    </div>
	  </section>

	  <section id="element-with-text">
	    <h2><a name="elements-with-text">Elements With Text</a></h2>
	    <p>Elements with immediate text nodes as children.</p>
	    <xsl:variable name="element-text-counts" as="element(count)*" select="$counts/text/count" />
	    <xsl:variable name="max-element-with-text-occ" select="max($element-text-counts)" />
	    <div class="counts element-colours">
	      <xsl:for-each select="$element-text-counts">
		<xsl:sort data-type="number" order="descending" select="xs:integer(.)" />
		<xsl:sort select="@name || ' ' || @ns" />
		<details>
		  <summary>
		    <div class="name">{dc:display-name(@name, @ns)}</div>
		    <div class="freq {
		      dc:percent-class(., $max-element-with-text-occ)
		    }">
		      <xsl:variable name="percent" as="xs:double"
		      select="99 * . div  $max-element-with-text-occ" />
		      <div class="value">{.}</div>
		      <div class="bar" style="width: {$percent}%;">&#160;</div>
		    </div>
		  </summary>

		  <!--* Details: how many docs? *-->
		  <h3>Frequency</h3>
		  <p>@<span class="name">{dc:full-name(@name, @ns)}</span> containing text directly was found in {@ndocs} document{if (@ndocs eq '1') then '' else 's'}.</p>
		</details>
	      </xsl:for-each>
	    </div>
	  </section>

	  <section id="attributes">
	    <h2><a name="attributes">Attributes</a></h2>
	    <xsl:variable name="max-attribute-occ" select="max($attribute-counts)" />
	    <div class="counts attribute-counts attribute-colours">
	      <xsl:for-each select="$attribute-counts">
		<xsl:sort data-type="number" order="descending" select="xs:integer(.)" />
		<xsl:sort select="@name || ' ' || @ns" />
		<details>
		  <summary>
		    <div class="name"><a name="a-{@name}"></a>{dc:display-name(@name, @ns)}</div>
		    <div class="freq {
		      dc:percent-class(., $max-attribute-occ)
		    }">
		      <xsl:variable name="percent" as="xs:double"
		      select="99 * . div  $max-attribute-occ" />
		      <div class="value">{.}</div>
		      <div class="bar" style="width: {$percent}%;">&#160;</div>
		    </div>
		  </summary>
		  <h3>Frequency</h3>
		  <p><span class="name">{dc:full-name(@name, @ns)}</span> was found in {@ndocs} document{if (@ndocs eq '1') then '' else 's'}.</p>
		  <h3>Parent elements of <span class="name">@{dc:display-name(@name, @ns)}</span></h3>
		  <ul>
		  <xsl:variable name="gi" select="@name" as="xs:string" />
		  <xsl:variable name="ns" select="(@ns, '')[1] " as="xs:string" />
		  <xsl:for-each-group
		    select="$attribute-value-counts[
		      (@name = $gi) and
		      (:note use of eq and = here as @ns may be () :)
		      (($ns eq '') or (@ns = $ns))
		    ]"
		    group-by="@parent-name || ' ' || @parent-ns">
		    <xsl:sort select="count(current-group())" data-type="number" order="descending" />
		    <xsl:sort select="current-grouping-key()" order="ascending" />

		    <li><span class="name">{
		    dc:full-name(
		      current-group()[1]/@parent-name,
		      current-group()[1]/@parent-ns
		    )}/</span><span class="name">{dc:display-name($gi, $ns)}</span> {sum(current-group())}</li>
		  </xsl:for-each-group>
		  </ul>
		</details>
	      </xsl:for-each><!--* attribute-counts *-->
	    </div><!--* counts *-->
	  </section>

	  <section id="attribute-values">
	    <h2><a name="attribute-values">Attribute Values</a></h2>
	    <p>Sorted by the number of distinct values seen.</p>
	    <!--* Here we want the number of count elements having the same
	        * value, so we'll use grouping and record how many of
		* each group we see:
		*-->
	    <xsl:variable name="group-lengths" as="xs:integer*">
	      <xsl:for-each-group select="$attribute-value-counts"
		group-by="@name || ' ' || @ns">

	        <xsl:sequence select="count(current-group())" />
	      </xsl:for-each-group>
	    </xsl:variable>
	    <!--* Now we know the most frequent attribute value, so we
	        * can draw a bar chart!
		*-->
	    <xsl:variable name="max-attribute-value-occ" as="xs:integer"
	    select="if (empty($group-lengths)) then 0 else max($group-lengths)" />
	    <div class="counts attribute-value-counts attribute-colours">
	      <xsl:choose>
		<xsl:when test="exists($attribute-value-counts[xs:integer(.) ge 1])">
		  <xsl:for-each-group select="$attribute-value-counts"
		    group-by="@name || ' ' || @ns">

		    <!--* sort by number of  distinct values: *-->
		    <xsl:sort data-type="number" order="descending"
		      select="count(current-group())" />
		    <xsl:sort select="@name || ' ' || @ns" />

		    <xsl:variable name="first" as="element(count)"
		      select="current-group()[1]" />

		    <!--* Each group has one or more count element for each
		        * distinct attribute value (more, if namespaces
			* or parents are included)
			*-->
		    <xsl:variable name="n-values" as="xs:integer"
		      select="count(distinct-values(current-group()/@value))" />

		    <xsl:if test="xs:integer(.) ge 1">
		      <details>
			<summary>
			  <div class="name">@{dc:display-name($first/@name, $first/@ns)}</div>
			  <div class="freq {
			    dc:percent-class($n-values, $max-attribute-value-occ)
			  }">
			    <xsl:variable name="percent" as="xs:double"
			      select="99 * $n-values div $max-attribute-value-occ" />
			    <div class="value">{$n-values}</div>
			    <div class="bar" style="width: {$percent}%;">&#160;</div>
			  </div>
			</summary>
			<div>
			  <h3>Frequency of Values</h3>
			  <p>Attribute @<span class="name">{dc:full-name(@name, @ns)}</span> was found in {@ndocs} document{if (@ndocs eq '1') then '' else 's'}
			  with {
			    if ($n-values eq 1)
			    then 'one' 
			    else $n-values
			  } distinct value{
			    if ($n-values eq 1)
			    then '' 
			    else 's'
			  }.</p>
			  <xsl:where-populated>
			    <h3>Values of @<span class="name">{dc:full-name(@name, @ns)}</span> occuring more than once</h3>
			  </xsl:where-populated>
			  <xsl:variable name="gi" select="@name" as="xs:string" />
			  <!--* the parents of this attribute *-->
			  <xsl:variable name="gi" select="@name" as="xs:string" />

			  <!--* so we have a sequence of count elements in
			      * current-group() all having the same name and ns,
			      * and no we want to make a line in the report for
			      * each value:
			      *-->
			  <xsl:for-each-group select="current-group()" group-by="@value">
			    <xsl:sort select="sum(current-group())" data-type="number" order="descending" />
			    <xsl:sort select="@name" order="ascending" />

			    <xsl:if test="sum(current-group()) gt 1">
			    <p>@{$gi} = {escape-html-uri(current-grouping-key())}: {sum(current-group())}</p>
			    </xsl:if>
			  </xsl:for-each-group>
			</div>
		      </details>
		    </xsl:if>
		  </xsl:for-each-group><!--* attribute-counts *-->
		</xsl:when>
		<xsl:otherwise>
		  <p>No attribute occurs more than once.</p>
		</xsl:otherwise>
	      </xsl:choose>
	    </div><!--* counts *-->
	  </section>

	  <p>[end of report]</p>
	</body>
      </html>
    </xsl:result-document>
    <p>HTML output written to {$html-output-uri}</p>
  </xsl:template>

  <xsl:function name="dc:filecheckunavailable" as="xs:boolean">
    <xsl:param name="filename" as="xs:string" />
    <xsl:sequence select="false()" />
  </xsl:function>

  <xsl:function name="dc:filecheck" as="function(xs:string) as xs:boolean?">
    <xsl:choose>
      <xsl:when test="function-available('file:exists')">
	<xsl:message>found file:exists#1</xsl:message>
	<xsl:sequence select="file:exists#1" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:sequence select="dc:filecheckunavailable#1" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:template name="make-collection-file">
    <xsl:param name="counts" as="element(freqx)" />

    <xsl:variable name="fileexists" select="dc:filecheck()" />
    <xsl:choose>
      <xsl:when test="$fileexists($write-collection-file-uri)">
        <p>Not overwriting {$write-collection-file-uri}</p>
      </xsl:when>
      <xsl:otherwise>
	<xsl:result-document href="{$write-collection-file-uri}" indent="yes">
	  <collection stable="false">
	    <xsl:text>{$NL}</xsl:text>
	    <xsl:comment>
	      <xsl:text>* Put a single doc element here for each{$NL}</xsl:text>
	      <xsl:text>    * document you want processed, with an {$NL}</xsl:text>
	      <xsl:text>    * href attribute pointing to the file; relative{$NL}</xsl:text>
	      <xsl:text>    * URLS like samples/boy.xml are relative to the{$NL}</xsl:text>
	      <xsl:text>    * collection file itself.{$NL}</xsl:text>
	      <xsl:text>    *</xsl:text>
	    </xsl:comment>
	    <xsl:text>{$NL}</xsl:text>
	    <xsl:for-each select="$counts/docs/doc">
	      <doc href="{@name}" />
	    </xsl:for-each>
	  </collection>
	</xsl:result-document>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
