FreqX - report on frequences of
* element names
* element parent names
* attribute names
* attribute element names
* attribute values

Run FreqX on
1. a directory (a folder) containing .xml files;
2. a single XML file;
3. a single XML file giving the names of files to analyze.

You can run freqx from within Oxygen XML Editor; use Oxygen to open
  README-running-in-oxygen.xml
for more information.

On OSX or Linux, or in "bash mode" on Windws, you can also run
  ./freqx.sh
directly, which may be simpler.

To try it from the commandline,
  ./freqx.sh -- collection=samples/listfile.xml
To get more information,
  ./freqx.sh --help

You can run freqx from another directory by giving the path to it, e.g.,
/home/churl9/programs/freqx-1.0/freqx.sh

You need to have a working Java runtime, and a copy of Saxon 9 or later.
If it doesn't work, see SUPPORT.txt for some things to try. The most
likely problem for JATS users is that you need to supply an XML Catalog
File, freqx.sh --catalog mycatalog.xml ... for example; SUPPORT.txt explains
how to do that, and a sample empty catalog file is included.

You can redistribute freqx if you like, but you must tell people where
to get it. See LICENSE.txt for more information.

Running FreqX

To analyze all XML files in a directory (a folder), set the parameter
collection
to the full path to that directory, e.g.
./freqx.sh -- collection=/home/charles/xmlfiles

By default, all XML files in sub-folders (directories inside that directory)
will be scanned. To contral that behaviour, set recurse to yes or no, where
yes emans to scan subfolders and no means to ignore them:
./freqx.sh -- collection=/home/charles/xmlfiles recurse=no

If you add a parameter pattern, only files matching that pattern are
included; the default is *.xml but you could use, for example,
./freqx.sh -- collection=/home/charles/xmlfiles pattern="*.(xml|svg)"
The quotes are needed for the Unix/Linux/Windows/Mac shell; don't add
them in Oxygen XML Editor.

If you prefer, you can supply a "manifest" or "collection" file; this
is an XML file with top-level element "collection" and "doc" children, each
having either an href attribute or an exclude attribute. To do this, set
collection to #none and collection-uri to point to a manifest file:
./freqx.sh -- collection="#none" collection-uri=manifest.xml

To make this easier, freqx can write out a manifest file for you:
./freqx.sh -- collection="#none" write-collection-file-uri="files.xml"
Then rename files.xml to manifest.xml (or whatever name you want) so it
doesn't get overwritten if you accidentally specify write-collection-file-uri
again.

Finally, you can run freqx on an Oxygen XMLEditor project file (*.xpr)
with,
./freqx.sh -- collection="#none" oxygen-project-file="/Users/george/my.xpr"
This will add all files explicitly listed in the XPR file that  freqx
was able to parse as XML; you could combine it with write-collection-file-uri
to get a starting-point for a manifest.

There are a lot of other parameters to freqx. They are listed near the start
of the XSLT file, freqx-iterate.xsl, and their current values are shown in
the HTML report.

For the HTML report, you can also supply your own CSS style file, using
the html-output-css-uri parameter. Try, for example,
./freqx.sh -- html-output-css-uri=lib/freqx-html-styles-black-yellow.css

## Licence

FreqX is open source software. Go barefoot for a day within a week of first using it. No
warranties or liabilities.

FreqX was originally developed with funding and direction from Mulberry Technologies,
https://www.mulberrytech.com/

If resolver.jar is included, it has an Apache licence.

Fonts Oswald and PT Sans are from Google Fonts, and you should check the licence there.

The word Licence is the English spelling; Americans should consider it to be a
license and obey it accordingly :)
