<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0" expand-text="yes"
  xmlns:dc="http://www.delightfulcomputing.com/"
  xmlns:err="http://www.w3.org/2005/xqt-errors"
  xmlns:file="http://expath.org/ns/file"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:saxon="http://saxon.sf.net/"
  exclude-result-prefixes="#all"
  >


  <!--* Read an XML document and return
      * a mix of e and at elements for each element or attribute
      *
      * This is meant to be called by freqx-iterate.xsl, which
      * is meant to be called by freqx.sh
      *-->

  <xsl:param name="dc:freqx-control-doc" as="element(freqx-control)?" select="()" />
  <xsl:variable name="freqx-control-doc" as="element(freqx-control)?" select="$dc:freqx-control-doc" />


  <xd:doc>
    <xd:p>Enter here or at the top level element:</xd:p>
  </xd:doc>
  <xsl:template match="/">

    <xsl:apply-templates select="*" />
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Constructs attribute nodes to capture the name
      of the given node and its parent as parent-node</xd:p>
    </xd:desc>
    <xd:param name="node">
      <xd:p>The node should be an element. You must handle the document node
      before calling this, as otherwise you would make duplicate
      entries for the top-level element and its ancestors.</xd:p>
    </xd:param>
  </xd:doc>
  <xsl:template name="make-parent-attributes" as="attribute(*)*">
    <xsl:param name="node" as="element(*)" />

    <xsl:attribute name="parent-name" select="local-name($node)" />
    <xsl:if test="namespace-uri($node)">
      <xsl:attribute name="parent-ns"
	select="namespace-uri($node)" />
    </xsl:if>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Match element nodes and turn them into un-nested e and
      (optionally) t elements so we can count them later.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="*">
    <xsl:variable name="rewrite" select="dc:get-rewrite(.)" />

        <xsl:if test="$rewrite">
      <xsl:message>rewrite {serialize($rewrite)}</xsl:message>
    </xsl:if>

    <xsl:if test="not( contains-token($rewrite/@drop, 'all') )">
      <!--* we want to know at least something about this element *-->
      <e name="{local-name(.)}">
	<xsl:if test="namespace-uri(.)">
	  <xsl:if test="not(contains-token($rewrite/@drop, 'namespace'))">
	    <xsl:attribute name="ns"
	      select="namespace-uri(.)" />
	  </xsl:if>
	</xsl:if>
	<xsl:if test="not(contains-token($rewrite/@drop, 'parent'))">
	  <xsl:choose>
	    <xsl:when test=".. instance of document-node()">
	      <xsl:attribute name="parent-name" select=" '#root' " />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:call-template name="make-parent-attributes">
		<xsl:with-param name="node" select=".." />
	      </xsl:call-template>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:if>
      </e>
    </xsl:if>
    <xsl:if test="not(contains-token($rewrite/@drop, 'text'))">
      <xsl:if test="text()[normalize-space(.)]">
	<t name="{local-name()}">
	  <xsl:if test="namespace-uri(.)">
	    <xsl:attribute name="ns"
	      select="namespace-uri(.)" />
	  </xsl:if>
	  <xsl:choose>
	    <xsl:when test=".. instance of document-node()">
	      <!--* root element has text in it, unusual but allowed *-->
	      <xsl:attribute name="parent-name" select=" '#root' " />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:call-template name="make-parent-attributes">
		<xsl:with-param name="node" select=".." />
	      </xsl:call-template>
	    </xsl:otherwise>
	  </xsl:choose>
	</t>
      </xsl:if>
    </xsl:if>
    <xsl:if test="not(contains-token($rewrite/@drop, 'attributes'))">
      <xsl:apply-templates select="@*" />
    </xsl:if>
    <xsl:if test="not(contains-token($rewrite/@drop, 'children'))">
      <!--* search for more elements inside this element: *-->
      <xsl:apply-templates select="*" />
    </xsl:if>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Match attribute nodes and turn them into
      <xd:i>at</xd:i> elements so we can count them later.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="@*">
    <xsl:variable name="rewrite" select="dc:get-rewrite(.)" />

    <!--* rewrite ns="*" name="ignore-me" drop="all" *-->
    <xsl:if test="not( contains-token($rewrite/@drop, 'all') )">
      <at name="{local-name(.)}">
        <xsl:if test="not(contains-token($rewrite/@drop, 'value'))">
	  <!--* use rewrite value="fixed" if given: *-->
	  <xsl:attribute name="value" select="($rewrite/@value, .)[1]" />
	</xsl:if>
	<xsl:if test="namespace-uri(.)">
	  <xsl:if test="not(contains-token($rewrite/@drop, 'namespace'))">
	    <xsl:attribute name="ns" select="namespace-uri(.)" />
	  </xsl:if>
	</xsl:if>
	<xsl:if test="not(contains-token($rewrite/@drop, 'parent'))">
	  <!--* ".." must be an element *-->
	  <xsl:for-each select="..">
	    <xsl:call-template name="make-parent-attributes">
	      <xsl:with-param name="node" select="." />
	    </xsl:call-template>
	  </xsl:for-each>
	</xsl:if>
      </at>
    </xsl:if>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Find a <xd:i>rewrite</xd:i> element that matches a
      given attribute node, or returns () if it  doesn't find
      one (for example, no attribute mapping is in effect)</xd:p>
    </xd:desc>
    <xd:param name="attnode"/>
  </xd:doc>
  <xsl:function name="dc:get-rewrite" as="element(rewrite)?">
    <xsl:param name="node" as="node()" />

    <!--* rewrite ns="*" name="id" drop="value" *-->

    <xsl:variable name="node-ns" select="xs:string(namespace-uri($node))" as="xs:string" />
    <!--* match a rewrite element if
        * there's no namespace name for the input attribute,
	* and none is requested,
	* or,
	* there's a namespace name for the input attribute,
	* and the same one was requested
	*
	* A missing ns attribute on a rewrite element will only match
	* attributes with no namespace URI.
	*-->
    <xsl:sequence
      select="$freqx-control-doc//attribute-overrides/rewrite[
	  (($node-ns ne '') and ((@ns = '*') or (@ns = $node-ns)))
	  or
	  (($node-ns eq '') and (not(@ns) or (@ns = '*') or (@ns = $node-ns)))
	][
	  (: there is always a name for $node :)
	  (not(@name) or (@name eq '*') or (@name eq local-name($node)))
	][
	  if (@element-namespace)
	  then
	    (: for an attribute, match the namespace of the element
	     : that bears the attribute.
	     :)
	    if ($node instance of attribute(*))
	    then @element-namespace = $node/../namespace-uri()
	    else
	      (: for an element, it's same as @ns i suppose :)
	      if ($node instance of element(*))
	      then @element-namespace = $node/namespace-uri()
	      else false() (: not an element or attribute :)
	  else true()
	][
	  if (@parent-namespace)
	  then
	    (: for an attribute, the same as element-namespace :)
	    if ($node instance of attribute(*))
	    then @parent-namespace = $node/../namespace-uri()
	    else (
	      (: for an element, it's the parent's namespace;
	       : is this really useful?
	       :)
	      if ($node instance of element(*))
	      then @parent-namespace = $node/../namespace-uri()
	      else false() (: not an element or attribute :)
	    )
	  else true()
	][
	  (: if there is more than one match take the first :)
	1
	]
      " />
  </xsl:function>
</xsl:stylesheet>
