#! /bin/sh

# Moderately portable shell script to run the freqx XSLT

ME=`basename $0`
DIR=`dirname $0`
case "$DIR" in
    "") DIR=`pwd`
	;;
esac

XSL="$DIR/freqx-iterate.xsl"

if java --version > /dev/null
then
    JAVA=java
elif test ! -z "$JAVA_HOME" -a -d "$JAVA_HOME"
then
    JAVA="$JAVA_HOME/bin/java"
fi

# some defaults

CATALOG="${DIR}/catalog.xml"
RESOLVER="${DIR}/resolver.jar"
SAXON="$HOME/packages/saxon-ee/saxon9ee.jar"

done_optons=
while test -z "$done_options"
do
    case "$1" in
	'')
	done_options=1
	;;
	--)
	    # splits options such as stylesheet parameters from files
	    done_options=1;
	    shift;
	    break; # exit the while loop
	    ;;
	--saxon)
	    if test ! -f "$2"
	    then
		echo "${ME}: error: Saxon JAR file not found, $2" 1>&2
		exit 1;
	    fi
	    SAXON="$2"
	    shift; shift
	    ;;
	--catalog)
	    if test ! -f "$2"
	    then
		echo "${ME}: error: catalog not found, $2" 1>&2
		exit 1;
	    fi
	    CATALOG="$2"
	    shift; shift;
	    ;;
	--resolver)
	    if test ! -f "$2" -a ! -f "${DIR}/$2"
	    then
		echo "${ME}: Java resolver "$2" not found here or in $DIR; try absolute path"  1>&2
		exit 1
	    fi
	    RESOLVER="$2"
	    shift; shift;
	    ;;
	--java)
	    if test "$2" --version > /dev/null
	    then
		JAVA="$2"
	    else
		echo "${ME}: could not run java "$2" --version" 1>&2
		exit 1
	    fi
	    shift; shift
	    ;;
	--xsl|--xslt)
	    if test -z "$2"
	    then
		echo "${ME}: $1 must be followed by the path to an XSLT stylesheet" 1>&2
		exit 1
	    elif test ! -f "$2"
	    then
		echo "${ME}: $1: stylesheet $2 not found" 1>&2
		exit 1
	    else
		XSL="$2"
	    fi
	    shift; shift;
	    ;;
	--help|--explain)
	    cat 1>&2 <<EOF
${ME} - run FreqX XSLT on input

Usage: ${ME} [options] [-- [stylesheet parameters]]

See FreqX.xslt for a description of the stylesheet parameters.
You will normally want to pass, at least,
${ME} -- collection=/some/folder/somewhere html-output-uri=report.html
Note the -- with spaces on either side before the first stylesheet parameter.

Options are
--help - print this message and exit

--saxon    "/absolute/path/to/saxon-ee.jar"
           You can use Saxon 9 or later, he, pe or ee edition;
	   file size stylesheet options might only work with ee.

--catalog  "/absolute/path/to/catalog.xml"
           Use the given XML file as an XML Catalog. There can be only one.
	   Current: $CATALOG

--resolver "/path/to/resolver.jar"
           Use the given Java JAR file as an XML Catalog resolver;
	   if the path is relative, it's relative to this script.
	   Current: $RESOLVER

--java     "/path/to/somewhere/bin/java"
           Use the program given to run Java; it must understand --version
	   Note: this is e.g. /usr/lib/jvm/bin/java, not  /usr/lib/jvm
	   Current: $JAVA

--xslt    "/path/to/alternate/XSLT/stylesheet.xsl"
          Use the given stylesheet instead of the default.
	  Current: $XSL

See the <xd:doc> section in
$XSL
for the stylesheet  parameters.
EOF
	    exit 0;
	    ;;
	*)
	    echo "Use ${ME} --help for how to use this script"  1>&2
	    echo "${ME}:  Use -- to separate program options from stylesheet options,"  1>&2
	    echo "e.g. ${ME} --java /usr/bin/java -- collection=/tmp" 1>&2
	    exit 2 # EX_USAGE
	    ;;
    esac
done

CLASSPATH="$SAXON"
SEP=`java -h 2>&1 | # Java help message gives us CLASSPATH separator
    grep "A [:;] separated" |  # extract it
    sed -e 's/^.*\([;:]\)  *sep.*$/\1/' -e 1q # take just the (first) one
`

if [ ! -z "$RESOLVER" ]
then
    CLASSPATH="${RESOLVER}${SEP}${SAXON}"
fi

set -x
"$JAVA" -cp "$CLASSPATH" net.sf.saxon.Transform -catalog:"$CATALOG" -it "$XSL" "$@"
