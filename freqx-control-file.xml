<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE freqx-control SYSTEM "lib/freqx-control.dtd">
<freqx-control version="1.0">
  <!--* This file lets you hide certain things from the FreqX reports,
      * or modify some defaults.
      *
      * You can suppress elements or attributes associated with a particular
      * XML Namespace, e.g. MathML or XLink, and you can replace attribute
      * values with a fixed string, e.g. so that id/idref attribute values all
      * become "#id" or "#idref" and sort together in the reports.
      *
      * Enable this processing with the FreqX stylesheet parameter
      * freqx-control-file=freqx-control-file.xml (or use a different
      * file, of course). If it's relative like this, it's sought for in the
      * same directory as the XSLT. Otherwise it will have a name starting
      * with a / or with file:// or even http:// if you want.
      *-->

  <settings />
      <!--* Nothing in settings right now; in principle you could override
          * stylesheet parameters here
	  *-->

  <attribute-overrides>
    <!--* Use this section to have FreqX ignore certain attributes, to ignore
	* all attributes with a certain namespace name, or to map attribute
	* values to a fixed value (so they all sort together, or for privacy).
	*
	* The rewrite elements have the following attributes:
	* @name - the name of the element to process specially;
	* @ns - the Namespace Name of the element (i.e. the URL,
	* EXACTLY byte for byte as it appears in the input documnts).
	* A rewrite element matches an attribute in the input if
	* (1) @ns is empty (ns="") or absent,
	* or,
	* (2) @ns is "*" to match any namespace,
	* or,
	* (3) @ns is exactly equal to the namespace URI of the attribute;
	*
	* and, in addition,
	* (a) @name is not given on the rewrite element,
	* or,
	* (b) @name is "*",
	* or,
	* (c) @name is equal to the local-name of the attribute (i.e.
	*     with any namespace prefix removed)
	*
	* The first match in this document will be used.
	*
	* And what can you do with rewrite elements?
	*
	* A @drop attribute can contain a space-separated list of
	* tokens, as follows:
	* value - remember the attribute was seen but not its value
	* namespace - ignore the namespace of the attribute itself,
	*    for the purpose of the report
	* parent - ignore the name of the element to which the attribute
	*    was affixed in the input
	* all - don't record anything about the attribute
	*
	* In addition, you can set @value to a value that will be
	* reported as if the attribute had that value in the input,
	* e.g. to map all ID values to the string "identifier" so that
	* they group together in the report, or to map XHTML "title" and
	* "alt" attributes to constant strings so as not to leak actual
	* content into the report, which might violate privacy or
	* confidentiality.
	*-->
    <rewrite ns="http://www.tei-c.org/ns/1.0" name="facs" value="1" />
    <rewrite name="facs" value="2" />
    <rewrite ns="*" name="facs" value="*" />
    <rewrite ns="*" name="Page" value="666" />

    <!--* Ignore all attributes on SVG elements;
        * note that because the first matching rewrite element is used,
	* if you move this rewrite to after the one for id attributes,
	* you will see id attributes even in the SVG namespace.
        *-->
    <rewrite element-namespace="http://www.w3.org/2000/svg" drop="all" />

    <!--* drop the values of ID, RID and IDREF attributes everywhere,
        * but still count the number of them for statistical purposes;
	* it's probably better to set them to a constant value, so we
	* do that for id here.
	*-->
    <rewrite ns="*" name="id" value="[id value suppressed in freqx-control-file.xml]" />
    <rewrite ns="*" name="rid" drop="value" />
    <rewrite ns="*" name="idref" drop="value" />
    <rewrite ns="*" name="href" value="[remote link]" />


  </attribute-overrides>

  <element-overrides>
    <!--* This is similar to attribute-overrides but for elements.
        * However, you can't change values (as that makes no sense),
	* and you _can_ tell FreqX to processes an element and attributes
	* but not its children, or to ignore the element altogether.
	*
	* A @drop attribute can contain a space-separated list of
	* tokens, as follows:
	* namespace - ignore the namespace of the attribute itself,
	*    for the purpose of the report
	* parent - ignore the name of the element to which the attribute
	*    was affixed in the input
	* text - don't record whether the element contained text
	* children - ignore all child elements (use together with text if
	*    you want to ignore text content too)
	* attributes - ignore all attributes on this element, overriding
	*    any attribute-overrides
	* all - don't record anything about the element
	*
	*-->

    <!--* We will process any element in the mathml namespace but ignore
        * its children. That way the only mml:* elements in the report will
	* be ones used at the top level:
	*-->
    <rewrite ns="http://www.w3.org/1998/Math/MathML" drop="children" />

  </element-overrides>

</freqx-control>
